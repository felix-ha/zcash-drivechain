Sample configuration files for:
```
SystemD: zsided.service
Upstart: zsided.conf
OpenRC:  zsided.openrc
         zsided.openrcconf
CentOS:  zsided.init
OS X:    org.bitcoin.zsided.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
