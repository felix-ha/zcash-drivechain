# Build steps for Linux

# 1. Install Dependencies

    sudo apt install -y build-essential libtool autotools-dev automake \
    pkg-config bsdmainutils python3 libevent-dev libboost-all-dev \
    libssl-dev libdb-dev libdb++-dev libsodium-dev curl git

## Optional: Install Qt dependencies (for GUI)

    sudo apt install -y libprotobuf-dev libqrencode-dev \
    libqt5core5a libqt5dbus5 libqt5gui5 protobuf-compiler \
    qttools5-dev qttools5-dev-tools

# 2. Download the Source Code

    git clone https://gitlab.com/CryptAxe/zcash-drivechain.git  

# 3. Fetch & setup zcash params (only needs to happen once per system)

    cd zcash-drivechain/
    zcutil/fetch-params.sh

# 4. Setup Rust (Once per system)

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    source $HOME/.cargo/env
    rustc --version  # Should output something like rustc 1.46.0 (04488afe3 2020-08-24)
 

# 5. Build

    ./autogen.sh
    ./configure --with-incompatible-bdb

# This directory should already exist but double check

    mkdir .cargo
    make

# 6. Run
    ./src/zsided or ./src/qt/zside-qt
